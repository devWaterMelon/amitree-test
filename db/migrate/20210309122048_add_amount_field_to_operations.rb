class AddAmountFieldToOperations < ActiveRecord::Migration[6.1]
  def change
    add_column :operations, :amount, :decimal, null: false
  end
end
