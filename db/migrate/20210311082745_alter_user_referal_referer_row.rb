class AlterUserReferalRefererRow < ActiveRecord::Migration[6.1]
  def change
    remove_index :user_referals, column: [:referer_uid, :new_uid]

    rename_column :user_referals, :referer_uid, :user_id
    rename_column :user_referals, :new_uid,     :new_user_id

    add_index :user_referals, [:user_id, :new_user_id], unique: true
  end
end
