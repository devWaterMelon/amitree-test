class InitTables < ActiveRecord::Migration[6.1]
  def change
    create_table :user_referals do |t|
      t.bigint :referer_uid, null: false
      t.bigint :new_uid,     null: false
      t.timestamps
    end

    create_table :operations do |t|
      t.belongs_to :user
      t.string     :type, null: false
      t.timestamps
    end

    add_index :user_referals, [:referer_uid, :new_uid], unique: true
  end
end
