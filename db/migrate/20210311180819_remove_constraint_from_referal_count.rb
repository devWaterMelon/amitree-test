class RemoveConstraintFromReferalCount < ActiveRecord::Migration[6.1]
  def change
    remove_check_constraint :users, name: "referal_count_check"
  end
end
