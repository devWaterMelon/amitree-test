class ReferalEarnJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    ReferalEarnInteraction.run(user_id: user_id)
  end

end
