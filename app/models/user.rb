class User < ApplicationRecord

  has_many :user_referals
  has_many :operations

  include Devisable
  include Referable
  include Balancable

end
