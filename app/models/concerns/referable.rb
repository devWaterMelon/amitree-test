module Referable

  extend ActiveSupport::Concern

  included do
    delegate :url_helpers, to: 'Rails.application.routes'
    def referal_link
      url_helpers.referal_sign_up_url referal_token, only_path: false
    end
  end

end
