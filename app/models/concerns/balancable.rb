module Balancable

  extend ActiveSupport::Concern

  class AddNegativeMoneyError < StandardError; end

  included do
    WELCOME_BONUS = 10.0
    REFERAL_BONUS = 10.0

    def give_welcome_bonus!
      add_money!(WELCOME_BONUS, Operation::REASONS.welcome)
    end

    def give_referal_bonus!
      add_money!(REFERAL_BONUS, Operation::REASONS.referal)
    end

    def add_money!(value, reason=nil)
      raise AddNegativeMoneyError if value <= 0

      amount = value.abs
      new_balance = balance + amount

      ActiveRecord::Base.transaction do
        update!(balance: new_balance)
        operations.create!(operation_type: reason, amount: amount)
      end
    end
  end

end
