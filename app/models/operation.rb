class Operation < ApplicationRecord
  belongs_to :user

  REASONS = OpenStruct.new({ welcome: 'welcome', referal: 'referal' })

  validates_inclusion_of :operation_type, in: ->(_) { REASONS.to_h.stringify_keys.keys }
end
