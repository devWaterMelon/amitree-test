class ReferalInteraction < BaseInteraction

  integer :user_id,       base:  10
  string  :referal_token, strip: true

  validates :referal_token, presence: true
  validate :check_user, :check_referer

  def execute

    ActiveRecord::Base.transaction do
      referer.user_referals.create!(new_user_id: new_user.id)
      new_user.give_welcome_bonus!
    end

    ReferalEarnJob.perform_later(referer.id)

  rescue StandardError => e
    msg = "Cant give welcome bonus to user: #{new_user.id} by token #{referal_token}"
    alert msg, e
  end


  private

  def referer
    @referer ||= User.find_by!(referal_token: referal_token)
  end

  def new_user
    @new_user ||= User.find(user_id)
  end

  def check_referer
    referer
  rescue ActiveRecord::RecordNotFound
    errors.add(:courier, 'Referer user not found')
  end

  def check_user
    new_user
  rescue ActiveRecord::RecordNotFound
    errors.add(:courier, 'New user not found')
  end

end
