module Lockable
  extend ActiveSupport::Concern

  LOCK_TIME = 2000 # ms

  class BusyInteractorError < StandardError
    def initialize(user_id)
      super("Busy interactor for: #{user_id}")
    end
  end

  included do
    private def lock_manager
      Redlock::Client.new([ ENV.fetch('REDIS_URL') { 'redis://localhost:6379/1' } ])
    end

    private def lock_key
      raise NotImplementedError
    end
  end

end
