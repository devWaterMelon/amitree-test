class ReferalEarnInteraction < BaseInteraction

  include Lockable

  integer :user_id, base: 10

  attr_accessor :new_referal_count
  attr_accessor :locked

  def execute
    @locked = lock_manager.lock(lock_key, LOCK_TIME)

    raise BusyInteractorError, user_id unless locked

    update_count_and_earn!

    lock_manager.unlock(locked) if locked

  rescue BusyInteractorError => e
    alert 'Referal earn interactor is busy right now', e
  rescue StandardError => e
    lock_manager.unlock locked

    msg = "Cant write referal earn for user: #{user_id}"
    alert msg, e
  end


  private

  def user
    @user ||= User.find user_id
  end

  def earn?
    (new_referal_count % 5).zero?
  end

  def set_new_referal_count
    @new_referal_count = user.referal_count + 1
  end

  def update_count_and_earn!
    set_new_referal_count

    ActiveRecord::Base.transaction do
      user.update!(referal_count: new_referal_count)
      user.give_referal_bonus! if earn?
    end
  end

  def lock_key
    "referal_earn_#{user_id}"
  end

end
