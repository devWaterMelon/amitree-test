class BaseInteraction < ActiveInteraction::Base

  private

  def alert(msg, error=nil)
    # Sentry, etc.
    Rails.logger.error [msg, error] # , error.backtrace.join("\n")
    errors.add(:error, msg)
  end

end
