class Api::V1::UserController < ApplicationController

  def info
    render json: {
      balance: current_user.balance,
      referal_link: current_user.referal_link,
    }
  end

end
