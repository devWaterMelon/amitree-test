require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  root to: 'application#index'

  if Rails.env.production?
    default_url_options host: ''
  else
    default_url_options host: 'localhost', port: 3000
  end

  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  devise_scope :user do
    get 'sign_up/:referal_token', to: 'users/registrations#new', as: 'referal_sign_up'
    get 'sign_up',                to: 'devise/registrations#new'
  end

  namespace :api do
    namespace 'v1' do
      get '/info', to: 'user#info'
      post '/users', action: 'create', controller: '/users/registrations'
    end
  end

end
