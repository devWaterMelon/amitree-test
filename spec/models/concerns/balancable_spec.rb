require 'rails_helper'

RSpec::Matchers.define_negated_matcher :not_change, :change

RSpec.describe Balancable do

  describe '#add_money!' do
    subject { user.add_money!(value, reason) }

    context 'success' do
      let(:user)   { create(:user) }
      let(:value)  { 100 }
      let(:reason) { Operation::REASONS.welcome }

      it {
        expect_any_instance_of(User).to                  receive(:update!).with(balance: value                       ).and_call_original
        expect_any_instance_of(user.operations.class).to receive(:create!).with(operation_type: reason, amount: value).and_call_original

        expect { subject }.to change(Operation, :count).by(1).and change { user.reload.balance }.by(value)
      }
    end

    context 'fail when trying to add negative' do
      let(:user)   { create(:user) }
      let(:value)  { -100 }
      let(:reason) { Operation::REASONS.welcome }

      it { expect { subject }.to raise_error Balancable::AddNegativeMoneyError }
    end


    context 'fail when wrong operation' do
      let(:user)   { create(:user) }
      let(:value)  { 100 }
      let(:reason) { 'strange_operation' }

      it {
        expect { subject }.to raise_error(ActiveRecord::RecordInvalid).and not_change { user.reload.balance }
      }
    end
  end

end
