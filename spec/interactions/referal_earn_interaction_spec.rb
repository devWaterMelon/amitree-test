require 'rails_helper'

RSpec.describe ReferalEarnInteraction do

  describe '#execute' do
    subject { described_class.run(**params) }

    context 'earn money' do
      let(:user) { create(:user, referal_count: 4) }
      let(:params) {{ user_id: user.id }}

      it {
        expect_any_instance_of(described_class).to receive(:update_count_and_earn!).and_call_original
        expect_any_instance_of(described_class).to receive(:set_new_referal_count).and_call_original

        expect_any_instance_of(User).to receive(:update!).twice.and_call_original
        expect_any_instance_of(User).to receive(:give_referal_bonus!).and_call_original

        expect { subject }.to change{ user.reload.balance }.by(User::REFERAL_BONUS)
        expect(subject.valid?).to be_truthy
      }
    end

    context 'dont earn money' do
      let(:user) { create(:user, referal_count: 2) }
      let(:params) {{ user_id: user.id }}

      it {
        expect_any_instance_of(described_class).to receive(:update_count_and_earn!).and_call_original
        expect_any_instance_of(described_class).to receive(:set_new_referal_count).and_call_original

        expect_any_instance_of(User).to     receive(:update!).and_call_original
        expect_any_instance_of(User).not_to receive(:give_referal_bonus!)

        expect { subject }.not_to change{ user.reload.balance }
        expect(subject.valid?).to be_truthy
      }
    end

    context 'concurrent earning' do
      let!(:user) { create(:user, referal_count: 2) }
      let(:params) {{ user_id: user.id }}

      it {
        threads = []
        results = []
        threads_count = 3
        sleep_seconds = 1

        allow_any_instance_of(described_class).to receive(:update_count_and_earn!) do
          sleep sleep_seconds
        end

        threads_count.times do
          threads << Thread.new {
            results << described_class.run(**params)
          }
        end
        threads.each { |t| t.join }

        skipped = results.select { |r| !r.valid? }.size

        expect(skipped).to eq threads_count - 1
      }
    end
  end

  describe 'private methods' do
    subject { described_class.run(**params) }

    context '#earn? is divide to 5' do
      let(:user) { create(:user, referal_count: 4) }
      let(:params) {{ user_id: user.id }}

      it { expect(subject.send(:earn?)).to be_truthy }
    end

    context '#earn? is not divide to 5' do
      let(:user) { create(:user, referal_count: 5) }
      let(:params) {{ user_id: user.id }}

      it { expect(subject.send(:earn?)).to be_falsy }
    end
  end

end
