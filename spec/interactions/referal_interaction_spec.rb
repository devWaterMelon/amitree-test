require 'rails_helper'

RSpec.describe ReferalInteraction do

  describe '#execute' do
    subject { described_class.run(**params) }

    context 'create referal user and give welcome bonus' do
      let(:referer) { create(:user) }
      let(:user) { create(:user) }
      let(:params) {{ user_id: user.id, referal_token: referer.referal_token }}

      it {
        expect_any_instance_of(described_class).to receive(:new_user).at_least(:once).and_return(user)
        expect(user).to receive(:give_welcome_bonus!).and_call_original

        expect { subject }.to change { referer.reload.user_referals.count }.by 1
        expect(subject.valid?).to be_truthy
      }
    end

    context 'raise when cant find referer' do
      let(:user) { create(:user) }
      let(:params) {{ user_id: user.id, referal_token: 'test' }}

      it { expect(subject.valid?).to be_falsy }
    end

    context 'raise when cant find new user' do
      let(:referer) { create(:user) }
      let(:params) {{ user_id: 99999, referal_token: referer.referal_token }}

      it { expect(subject.valid?).to be_falsy }
    end
  end

end
