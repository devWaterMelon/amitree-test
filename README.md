# README

[task pdf](https://gitlab.com/devWaterMelon/amitree-test/-/blob/master/Referral_Design_Take_Home_Interview.pdf)

## Design decisions
When Alice gives a referal link to Bob and Bob sign's up via this link I give welcome bonus to Bob and add Bob to Alice referals list ([referal_interaction.rb](https://gitlab.com/devWaterMelon/amitree-test/-/blob/master/app/interactions/referal_interaction.rb))

After this I run [ReferalEarnJob](https://gitlab.com/devWaterMelon/amitree-test/-/blob/master/app/jobs/referal_earn_job.rb) which give money for 5 referals to Alice asynchronously using Sidekiq ([referal_earn_interaction.rb](https://gitlab.com/devWaterMelon/amitree-test/-/blob/master/app/interactions/referal_earn_interaction.rb)). I'm using redlock lock to make sure that I give the money once per 5 registrations. All others concurrent sidekiq workers will repeat their job after lock realizing.

I give money for every 5 registrations via refaral link, but it's easy to change behaviour: you need to change ```def earn?``` function in [referal_earn_interaction.rb](https://gitlab.com/devWaterMelon/amitree-test/-/blob/master/app/interactions/referal_earn_interaction.rb)

For authentication I use [Devise](https://github.com/heartcombo/devise)
## API
API has dead simple versioning mechanism: version in url. Like: */api/**v1**/smth* or */api/**v99**/smthnew*

```GET /api/v1/info```
* Params: empty
* Description: Returns users info: balance and referal link


```POST /api/v1/users```
* Params:
```json
// required
"user": {
  "email": "",
  "password": "",
  "password_confirmation": "",
}
// optional
"referal_token": ""
```
* Description: Creates new user, this link is a reference for Devise url ```POST /users```

## Run
You can use docker-compose, which runs Rails, Sidekiq, Postgres and Redis in containers.
```shell
$ docker-compose build # run it once

$ docker-compose up
```

When run manually:
* install ruby 2.7.2, bundler 2.2.7, nodejs 15, redis, postgres (i'm using version 13)

## Testing
For tests you need to start Redis (manually or Docker), because some tests are checking redlocks

Run specs in docker container
```shell
$ docker exec -it <CONTAINER ID> /bin/bash

$ RAILS_ENV=test rspec spec
```
