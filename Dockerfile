ARG RUBY_VERSION

FROM ruby:$RUBY_VERSION-slim-buster

ARG NODE_VERSION
ARG BUNDLER_VERSION

# Common dependencies
RUN apt-get update -qq \
  && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends build-essential gnupg2 curl git vim ssh imagemagick ffmpeg

RUN echo "ruby: ${RUBY_VERSION}, node: ${NODE_VERSION}, bundler: ${BUNDLER_VERSION}"

# Add NodeJS to sources list
RUN curl -fsSL https://deb.nodesource.com/setup_$NODE_VERSION.x | bash -

# Add Yarn to the sources list
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo 'deb http://dl.yarnpkg.com/debian/ stable main' > /etc/apt/sources.list.d/yarn.list

RUN apt-get update -qq \
  && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
    nodejs \
    yarn \
    libpq-dev \
  && apt-get clean \
  && rm -rf /var/cache/apt/archives/* \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && truncate -s 0 /var/log/*log

# Configure bundler
ENV LANG=C.UTF-8 \
  BUNDLE_JOBS=4 \
  BUNDLE_RETRY=3

# Upgrade RubyGems and install required Bundler version
RUN gem update --system && gem install bundler:$BUNDLER_VERSION


# # Uncomment this line if you want to run binstubs without prefixing with `bin/` or `bundle exec`
ENV PATH /app/bin:$PATH

EXPOSE 3000
EXPOSE 3035
